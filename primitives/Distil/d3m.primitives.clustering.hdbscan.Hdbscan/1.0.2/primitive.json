{
    "id": "ca014488-6004-4b54-9403-5920fbe5a834",
    "version": "1.0.2",
    "name": "hdbscan",
    "keywords": [
        "Clustering"
    ],
    "source": {
        "name": "Distil",
        "contact": "mailto:jeffrey.gleason@kungfu.ai",
        "uris": [
            "https://github.com/kungfuai/d3m-primitives"
        ]
    },
    "installation": [
        {
            "type": "PIP",
            "package": "cython",
            "version": "0.29.16"
        },
        {
            "type": "PIP",
            "package_uri": "git+https://github.com/kungfuai/d3m-primitives.git@66a2eac2d8f31fe3d892ffc5f1b7a56c4fbfd6af#egg=kf-d3m-primitives"
        }
    ],
    "python_path": "d3m.primitives.clustering.hdbscan.Hdbscan",
    "algorithm_types": [
        "DBSCAN"
    ],
    "primitive_family": "CLUSTERING",
    "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/primitive.json",
    "original_python_path": "kf_d3m_primitives.clustering.hdbscan.Hdbscan.HdbscanPrimitive",
    "primitive_code": {
        "class_type_arguments": {
            "Inputs": "d3m.container.pandas.DataFrame",
            "Outputs": "d3m.container.pandas.DataFrame",
            "Hyperparams": "kf_d3m_primitives.clustering.hdbscan.Hdbscan.Hyperparams",
            "Params": "NoneType"
        },
        "interfaces_version": "2020.11.3",
        "interfaces": [
            "transformer.TransformerPrimitiveBase",
            "base.PrimitiveBase"
        ],
        "hyperparams": {
            "algorithm": {
                "type": "d3m.metadata.hyperparams.Enumeration",
                "default": "HDBSCAN",
                "structural_type": "str",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "type of clustering algorithm to use",
                "values": [
                    "DBSCAN",
                    "HDBSCAN"
                ]
            },
            "eps": {
                "type": "d3m.metadata.hyperparams.Uniform",
                "default": 0.5,
                "structural_type": "float",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "maximum distance between two samples for them to be considered as in         the same neigborhood, used in DBSCAN algorithm",
                "lower": 0,
                "upper": 9223372036854775807,
                "lower_inclusive": true,
                "upper_inclusive": false
            },
            "min_cluster_size": {
                "type": "d3m.metadata.hyperparams.UniformInt",
                "default": 5,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "the minimum size of clusters",
                "lower": 2,
                "upper": 9223372036854775807,
                "lower_inclusive": true,
                "upper_inclusive": false
            },
            "min_samples": {
                "type": "d3m.metadata.hyperparams.UniformInt",
                "default": 5,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "The number of samples in a neighbourhood for a point to be considered a core point.",
                "lower": 1,
                "upper": 9223372036854775807,
                "lower_inclusive": true,
                "upper_inclusive": false
            },
            "cluster_selection_method": {
                "type": "d3m.metadata.hyperparams.Enumeration",
                "default": "eom",
                "structural_type": "str",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "Determines how clusters are selected from the cluster hierarchy tree for HDBSCAN",
                "values": [
                    "leaf",
                    "eom"
                ]
            },
            "required_output": {
                "type": "d3m.metadata.hyperparams.Enumeration",
                "default": "feature",
                "structural_type": "str",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "Determines whether the output is a dataframe with just predictions,            or an additional feature added to the input dataframe.",
                "values": [
                    "prediction",
                    "feature"
                ]
            }
        },
        "arguments": {
            "hyperparams": {
                "type": "kf_d3m_primitives.clustering.hdbscan.Hdbscan.Hyperparams",
                "kind": "RUNTIME"
            },
            "random_seed": {
                "type": "int",
                "kind": "RUNTIME",
                "default": 0
            },
            "timeout": {
                "type": "typing.Union[NoneType, float]",
                "kind": "RUNTIME",
                "default": null
            },
            "iterations": {
                "type": "typing.Union[NoneType, int]",
                "kind": "RUNTIME",
                "default": null
            },
            "produce_methods": {
                "type": "typing.Sequence[str]",
                "kind": "RUNTIME"
            },
            "inputs": {
                "type": "d3m.container.pandas.DataFrame",
                "kind": "PIPELINE"
            },
            "params": {
                "type": "NoneType",
                "kind": "RUNTIME"
            }
        },
        "class_methods": {},
        "instance_methods": {
            "__init__": {
                "kind": "OTHER",
                "arguments": [
                    "hyperparams",
                    "random_seed"
                ],
                "returns": "NoneType"
            },
            "fit": {
                "kind": "OTHER",
                "arguments": [
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[NoneType]",
                "description": "A noop.\n\nParameters\n----------\ntimeout:\n    A maximum time this primitive should be fitting during this method call, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nA ``CallResult`` with ``None`` value."
            },
            "fit_multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling ``fit`` and after that multiple produce methods at once.\n\nParameters\n----------\nproduce_methods:\n    A list of names of produce methods to call.\ninputs:\n    The inputs given to all produce methods.\ntimeout:\n    A maximum time this primitive should take to both fit the primitive and produce outputs\n    for all produce methods listed in ``produce_methods`` argument, in seconds.\niterations:\n    How many of internal iterations should the primitive do for both fitting and producing\n    outputs of all produce methods.\n\nReturns\n-------\nA dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "get_params": {
                "kind": "OTHER",
                "arguments": [],
                "returns": "NoneType",
                "description": "A noop.\n\nReturns\n-------\nAn instance of parameters."
            },
            "multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling multiple produce methods at once.\n\nWhen a primitive has multiple produce methods it is common that they might compute the\nsame internal results for same inputs but return different representations of those results.\nIf caller is interested in multiple of those representations, calling multiple produce\nmethods might lead to recomputing same internal results multiple times. To address this,\nthis method allows primitive author to implement an optimized version which computes\ninternal results only once for multiple calls of produce methods, but return those different\nrepresentations.\n\nIf any additional method arguments are added to primitive's produce method(s), they have\nto be added to this method as well. This method should accept an union of all arguments\naccepted by primitive's produce method(s) and then use them accordingly when computing\nresults. Despite accepting all arguments they can be passed as ``None`` by the caller\nwhen they are not needed by any of the produce methods in ``produce_methods``.\n\nThe default implementation of this method just calls all produce methods listed in\n``produce_methods`` in order and is potentially inefficient.\n\nIf primitive should have been fitted before calling this method, but it has not been,\nprimitive should raise a ``PrimitiveNotFittedError`` exception.\n\nParameters\n----------\nproduce_methods:\n    A list of names of produce methods to call.\ninputs:\n    The inputs given to all produce methods.\ntimeout:\n    A maximum time this primitive should take to produce outputs for all produce methods\n    listed in ``produce_methods`` argument, in seconds.\niterations:\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nA dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "produce": {
                "kind": "PRODUCE",
                "arguments": [
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[d3m.container.pandas.DataFrame]",
                "singleton": false,
                "inputs_across_samples": [],
                "description": "Produce primitive's best choice of the output for each of the inputs.\n\nThe output value should be wrapped inside ``CallResult`` object before returning.\n\nIn many cases producing an output is a quick operation in comparison with ``fit``, but not\nall cases are like that. For example, a primitive can start a potentially long optimization\nprocess to compute outputs. ``timeout`` and ``iterations`` can serve as a way for a caller\nto guide the length of this process.\n\nIdeally, a primitive should adapt its call to try to produce the best outputs possible\ninside the time allocated. If this is not possible and the primitive reaches the timeout\nbefore producing outputs, it should raise a ``TimeoutError`` exception to signal that the\ncall was unsuccessful in the given time. The state of the primitive after the exception\nshould be as the method call has never happened and primitive should continue to operate\nnormally. The purpose of ``timeout`` is to give opportunity to a primitive to cleanly\nmanage its state instead of interrupting execution from outside. Maintaining stable internal\nstate should have precedence over respecting the ``timeout`` (caller can terminate the\nmisbehaving primitive from outside anyway). If a longer ``timeout`` would produce\ndifferent outputs, then ``CallResult``'s ``has_finished`` should be set to ``False``.\n\nSome primitives have internal iterations (for example, optimization iterations).\nFor those, caller can provide how many of primitive's internal iterations\nshould a primitive do before returning outputs. Primitives should make iterations as\nsmall as reasonable. If ``iterations`` is ``None``, then there is no limit on\nhow many iterations the primitive should do and primitive should choose the best amount\nof iterations on its own (potentially controlled through hyper-parameters).\nIf ``iterations`` is a number, a primitive has to do those number of iterations,\nif possible. ``timeout`` should still be respected and potentially less iterations\ncan be done because of that. Primitives with internal iterations should make\n``CallResult`` contain correct values.\n\nFor primitives which do not have internal iterations, any value of ``iterations``\nmeans that they should run fully, respecting only ``timeout``.\n\nIf primitive should have been fitted before calling this method, but it has not been,\nprimitive should raise a ``PrimitiveNotFittedError`` exception.\n\nParameters\n----------\ninputs: D3M dataframe with attached metadata for semi-supervised or unsupervised data\n\nReturns\n-------\nOutputs:\n    The output depends on the required_output hyperparameter and is either a dataframe\n    containing a single column where each entry is the cluster ID, or the input daatframe\n    with the cluster ID of each row added as an additional feature."
            },
            "set_params": {
                "kind": "OTHER",
                "arguments": [
                    "params"
                ],
                "returns": "NoneType",
                "description": "A noop.\n\nParameters\n----------\nparams:\n    An instance of parameters."
            },
            "set_training_data": {
                "kind": "OTHER",
                "arguments": [],
                "returns": "NoneType",
                "description": "A noop.\n\nParameters\n----------"
            }
        },
        "class_attributes": {
            "logger": "logging.Logger",
            "metadata": "d3m.metadata.base.PrimitiveMetadata"
        },
        "instance_attributes": {
            "hyperparams": "d3m.metadata.hyperparams.Hyperparams",
            "random_seed": "int",
            "docker_containers": "typing.Dict[str, d3m.primitive_interfaces.base.DockerContainer]",
            "volumes": "typing.Dict[str, str]",
            "temporary_directory": "typing.Union[NoneType, str]"
        }
    },
    "structural_type": "kf_d3m_primitives.clustering.hdbscan.Hdbscan.HdbscanPrimitive",
    "description": "This primitive applies hierarchical density-based and density-based clustering algorithms.\n\nAttributes\n----------\nmetadata:\n    Primitive's metadata. Available as a class attribute.\nlogger:\n    Primitive's logger. Available as a class attribute.\nhyperparams:\n    Hyperparams passed to the constructor.\nrandom_seed:\n    Random seed passed to the constructor.\ndocker_containers:\n    A dict mapping Docker image keys from primitive's metadata to (named) tuples containing\n    container's address under which the container is accessible by the primitive, and a\n    dict mapping exposed ports to ports on that address.\nvolumes:\n    A dict mapping volume keys from primitive's metadata to file and directory paths\n    where downloaded and extracted files are available to the primitive.\ntemporary_directory:\n    An absolute path to a temporary directory a primitive can use to store any files\n    for the duration of the current pipeline run phase. Directory is automatically\n    cleaned up after the current pipeline run phase finishes.",
    "digest": "a398600fc6b9917b4716dca491134e3c72c823691462e474bdb69f50c0c8bc76"
}
