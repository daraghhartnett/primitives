{
    "id": "8f91951f-7c83-4dd4-ad00-16d12bb964eb",
    "version": "0.1.0",
    "name": "Convolutional Neural Network",
    "description": "Convolutional Neural Network primitive using PyTorch framework.\nUsed to extract deep features from images.\nIt can be used as a pre-trained feature extractor, to extract features from\nconvolutional layers or the fully connected layers by setting include_top.\nIt can also be fine-tunned to fit new data, by setting feature extraction to False.\nAvailable pre-trained CNN models are:\n  - VGG-16\n  - VGG-16 with Batch-Norm\n  - GoogLeNet\n  - ResNeT\n  - MobileNet (A Light weight CNN model)\nAll available models are pre-trained on ImageNet.\n\nAttributes\n----------\nmetadata : PrimitiveMetadata\n    Primitive's metadata. Available as a class attribute.\nlogger : Logger\n    Primitive's logger. Available as a class attribute.\nhyperparams : Hyperparams\n    Hyperparams passed to the constructor.\nrandom_seed : int\n    Random seed passed to the constructor.\ndocker_containers : Dict[str, DockerContainer]\n    A dict mapping Docker image keys from primitive's metadata to (named) tuples containing\n    container's address under which the container is accessible by the primitive, and a\n    dict mapping exposed ports to ports on that address.\nvolumes : Dict[str, str]\n    A dict mapping volume keys from primitive's metadata to file and directory paths\n    where downloaded and extracted files are available to the primitive.\ntemporary_directory : str\n    An absolute path to a temporary directory a primitive can use to store any files\n    for the duration of the current pipeline run phase. Directory is automatically\n    cleaned up after the current pipeline run phase finishes.",
    "python_path": "d3m.primitives.feature_extraction.cnn.UBC",
    "primitive_family": "FEATURE_EXTRACTION",
    "algorithm_types": [
        "CONVOLUTIONAL_NEURAL_NETWORK"
    ],
    "source": {
        "name": "UBC",
        "contact": "mailto:tonyjos@ubc.cs.ca",
        "uris": [
            "https://github.com/plai-group/ubc_primitives.git"
        ]
    },
    "keywords": [
        "cnn",
        "vgg",
        "googlenet",
        "resnet",
        "mobilenet",
        "convolutional neural network",
        "deep learning"
    ],
    "installation": [
        {
            "type": "PIP",
            "package_uri": "git+https://github.com/plai-group/ubc_primitives.git@dfccdc276d3f2ed18a04b9bb69795b87505339a5#egg=ubc_primitives"
        },
        {
            "type": "FILE",
            "key": "vgg16-397923af.pth",
            "file_uri": "https://download.pytorch.org/models/vgg16-397923af.pth",
            "file_digest": "397923af8e79cdbb6a7127f12361acd7a2f83e06b05044ddf496e83de57a5bf0"
        },
        {
            "type": "FILE",
            "key": "vgg16_bn-6c64b313.pth",
            "file_uri": "https://download.pytorch.org/models/vgg16_bn-6c64b313.pth",
            "file_digest": "6c64b3138f2f4fcb3bcc4cafde11619c4f440eb1631787e93a682fd88305888a"
        },
        {
            "type": "FILE",
            "key": "googlenet-1378be20.pth",
            "file_uri": "https://download.pytorch.org/models/googlenet-1378be20.pth",
            "file_digest": "1378be20a8e875cf1568b8a71654e704449655e34711a959a38b04fb34905cef"
        },
        {
            "type": "FILE",
            "key": "mobilenet_v2-b0353104.pth",
            "file_uri": "https://download.pytorch.org/models/mobilenet_v2-b0353104.pth",
            "file_digest": "b03531047ffacf1e2488318dcd2aba1126cde36e3bfe1aa5cb07700aeeee9889"
        },
        {
            "type": "FILE",
            "key": "resnet34-333f7ec4.pth",
            "file_uri": "https://download.pytorch.org/models/resnet34-333f7ec4.pth",
            "file_digest": "333f7ec4c6338da2cbed37f1fc0445f9624f1355633fa1d7eab79a91084c6cef"
        }
    ],
    "hyperparams_to_tune": [
        "learning_rate",
        "cnn_type"
    ],
    "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/primitive.json",
    "original_python_path": "primitives.cnn.cnn.ConvolutionalNeuralNetwork",
    "primitive_code": {
        "class_type_arguments": {
            "Inputs": "d3m.container.pandas.DataFrame",
            "Outputs": "d3m.container.pandas.DataFrame",
            "Params": "primitives.cnn.cnn.Params",
            "Hyperparams": "primitives.cnn.cnn.Hyperparams"
        },
        "interfaces_version": "2020.1.9",
        "interfaces": [
            "supervised_learning.SupervisedLearnerPrimitiveBase",
            "base.PrimitiveBase"
        ],
        "hyperparams": {
            "use_pretrained": {
                "type": "d3m.metadata.hyperparams.UniformBool",
                "default": true,
                "structural_type": "bool",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "Whether to use pre-trained ImageNet weights"
            },
            "train_endToend": {
                "type": "d3m.metadata.hyperparams.UniformBool",
                "default": false,
                "structural_type": "bool",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "Whether to train the network end to end or fine-tune the last layer only."
            },
            "use_batch_norm": {
                "type": "d3m.metadata.hyperparams.UniformBool",
                "default": false,
                "structural_type": "bool",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "Whether to use batch norm for VGG network"
            },
            "feature_extract_only": {
                "type": "d3m.metadata.hyperparams.UniformBool",
                "default": true,
                "structural_type": "bool",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "Whether to use CNN as feature extraction only without training"
            },
            "include_top": {
                "type": "d3m.metadata.hyperparams.UniformBool",
                "default": true,
                "structural_type": "bool",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "Whether to use top layers, i.e. final fully connected layers"
            },
            "img_resize": {
                "type": "d3m.metadata.hyperparams.Hyperparameter",
                "default": 224,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "Size to resize the input image"
            },
            "output_dim": {
                "type": "d3m.metadata.hyperparams.Hyperparameter",
                "default": 1000,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "Dimensions of CNN output."
            },
            "last_activation_type": {
                "type": "d3m.metadata.hyperparams.Enumeration",
                "default": "linear",
                "structural_type": "str",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "Type of activation (non-linearity) following the last layer.",
                "values": [
                    "linear",
                    "relu",
                    "tanh",
                    "sigmoid",
                    "softmax"
                ]
            },
            "cnn_type": {
                "type": "d3m.metadata.hyperparams.Enumeration",
                "default": "resnet",
                "structural_type": "str",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "Type of convolutional neural network to use.",
                "values": [
                    "vgg",
                    "googlenet",
                    "mobilenet",
                    "resnet"
                ]
            },
            "loss_type": {
                "type": "d3m.metadata.hyperparams.Enumeration",
                "default": "mse",
                "structural_type": "str",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "Type of loss used for the local training (fit) of this primitive.",
                "values": [
                    "mse",
                    "crossentropy",
                    "l1"
                ]
            },
            "optimizer_type": {
                "type": "d3m.metadata.hyperparams.Enumeration",
                "default": "adam",
                "structural_type": "str",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "Type of optimizer used during training (fit).",
                "values": [
                    "adam",
                    "sgd"
                ]
            },
            "minibatch_size": {
                "type": "d3m.metadata.hyperparams.Hyperparameter",
                "default": 32,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "Minibatch size used during training (fit)."
            },
            "learning_rate": {
                "type": "d3m.metadata.hyperparams.Hyperparameter",
                "default": 0.0001,
                "structural_type": "float",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "Learning rate used during training (fit)."
            },
            "momentum": {
                "type": "d3m.metadata.hyperparams.Hyperparameter",
                "default": 0.9,
                "structural_type": "float",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "Momentum used during training (fit), only for optimizer_type sgd."
            },
            "weight_decay": {
                "type": "d3m.metadata.hyperparams.Hyperparameter",
                "default": 0.0001,
                "structural_type": "float",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "Weight decay (L2 regularization) used during training (fit)."
            },
            "shuffle": {
                "type": "d3m.metadata.hyperparams.UniformBool",
                "default": true,
                "structural_type": "bool",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "Shuffle minibatches in each epoch of training (fit)."
            },
            "fit_threshold": {
                "type": "d3m.metadata.hyperparams.Hyperparameter",
                "default": 1e-05,
                "structural_type": "float",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "Threshold of loss value to early stop training (fit)."
            },
            "num_iterations": {
                "type": "d3m.metadata.hyperparams.Hyperparameter",
                "default": 100,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "Number of iterations to train the model."
            }
        },
        "arguments": {
            "hyperparams": {
                "type": "primitives.cnn.cnn.Hyperparams",
                "kind": "RUNTIME"
            },
            "volumes": {
                "type": "typing.Union[NoneType, typing.Dict[str, str]]",
                "kind": "RUNTIME",
                "default": null
            },
            "timeout": {
                "type": "typing.Union[NoneType, float]",
                "kind": "RUNTIME",
                "default": null
            },
            "iterations": {
                "type": "typing.Union[NoneType, int]",
                "kind": "RUNTIME",
                "default": null
            },
            "produce_methods": {
                "type": "typing.Sequence[str]",
                "kind": "RUNTIME"
            },
            "inputs": {
                "type": "d3m.container.pandas.DataFrame",
                "kind": "PIPELINE"
            },
            "outputs": {
                "type": "d3m.container.pandas.DataFrame",
                "kind": "PIPELINE"
            },
            "params": {
                "type": "primitives.cnn.cnn.Params",
                "kind": "RUNTIME"
            }
        },
        "class_methods": {},
        "instance_methods": {
            "__init__": {
                "kind": "OTHER",
                "arguments": [
                    "hyperparams",
                    "volumes"
                ],
                "returns": "typing.Any"
            },
            "fit": {
                "kind": "OTHER",
                "arguments": [
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[NoneType]",
                "description": "Inputs: Dataset dataFrame\nReturns: None\n\nParameters\n----------\ntimeout : float\n    A maximum time this primitive should be fitting during this method call, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nCallResult[None]\n    A ``CallResult`` with ``None`` value."
            },
            "fit_multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "outputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling ``fit`` and after that multiple produce methods at once.\n\nThis method allows primitive author to implement an optimized version of both fitting\nand producing a primitive on same data.\n\nIf any additional method arguments are added to primitive's ``set_training_data`` method\nor produce method(s), or removed from them, they have to be added to or removed from this\nmethod as well. This method should accept an union of all arguments accepted by primitive's\n``set_training_data`` method and produce method(s) and then use them accordingly when\ncomputing results.\n\nThe default implementation of this method just calls first ``set_training_data`` method,\n``fit`` method, and all produce methods listed in ``produce_methods`` in order and is\npotentially inefficient.\n\nParameters\n----------\nproduce_methods : Sequence[str]\n    A list of names of produce methods to call.\ninputs : Inputs\n    The inputs given to ``set_training_data`` and all produce methods.\noutputs : Outputs\n    The outputs given to ``set_training_data``.\ntimeout : float\n    A maximum time this primitive should take to both fit the primitive and produce outputs\n    for all produce methods listed in ``produce_methods`` argument, in seconds.\niterations : int\n    How many of internal iterations should the primitive do for both fitting and producing\n    outputs of all produce methods.\n\nReturns\n-------\nMultiCallResult\n    A dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "get_params": {
                "kind": "OTHER",
                "arguments": [],
                "returns": "primitives.cnn.cnn.Params",
                "description": "Returns parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nParameters should include all data which is necessary to create a new instance of this primitive\nbehaving exactly the same as this instance, when the new instance is created by passing the same\nparameters to the class constructor and calling ``set_params``.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nReturns\n-------\nParams\n    An instance of parameters."
            },
            "multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling multiple produce methods at once.\n\nWhen a primitive has multiple produce methods it is common that they might compute the\nsame internal results for same inputs but return different representations of those results.\nIf caller is interested in multiple of those representations, calling multiple produce\nmethods might lead to recomputing same internal results multiple times. To address this,\nthis method allows primitive author to implement an optimized version which computes\ninternal results only once for multiple calls of produce methods, but return those different\nrepresentations.\n\nIf any additional method arguments are added to primitive's produce method(s), they have\nto be added to this method as well. This method should accept an union of all arguments\naccepted by primitive's produce method(s) and then use them accordingly when computing\nresults.\n\nThe default implementation of this method just calls all produce methods listed in\n``produce_methods`` in order and is potentially inefficient.\n\nIf primitive should have been fitted before calling this method, but it has not been,\nprimitive should raise a ``PrimitiveNotFittedError`` exception.\n\nParameters\n----------\nproduce_methods : Sequence[str]\n    A list of names of produce methods to call.\ninputs : Inputs\n    The inputs given to all produce methods.\ntimeout : float\n    A maximum time this primitive should take to produce outputs for all produce methods\n    listed in ``produce_methods`` argument, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nMultiCallResult\n    A dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "produce": {
                "kind": "PRODUCE",
                "arguments": [
                    "inputs",
                    "iterations",
                    "timeout"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[d3m.container.pandas.DataFrame]",
                "singleton": false,
                "inputs_across_samples": [],
                "description": "Inputs: Dataset dataFrame\nReturns: Pandas DataFramefor for classification or regression task\n\nParameters\n----------\ninputs : Inputs\n    The inputs of shape [num_inputs, ...].\ntimeout : float\n    A maximum time this primitive should take to produce outputs during this method call, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nCallResult[Outputs]\n    The outputs of shape [num_inputs, ...] wrapped inside ``CallResult``."
            },
            "set_params": {
                "kind": "OTHER",
                "arguments": [
                    "params"
                ],
                "returns": "NoneType",
                "description": "Sets parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nParameters\n----------\nparams : Params\n    An instance of parameters."
            },
            "set_training_data": {
                "kind": "OTHER",
                "arguments": [
                    "inputs",
                    "outputs"
                ],
                "returns": "NoneType",
                "description": "Sets current training data of this primitive.\n\nThis marks training data as changed even if new training data is the same as\nprevious training data.\n\nStandard sublasses in this package do not adhere to the Liskov substitution principle when\ninheriting this method because they do not necessary accept all arguments found in the base\nclass. This means that one has to inspect which arguments are accepted at runtime, or in\nother words, one has to inspect which exactly subclass a primitive implements, if\nyou are accepting a wider range of primitives. This relaxation is allowed only for\nstandard subclasses found in this package. Primitives themselves should not break\nthe Liskov substitution principle but should inherit from a suitable base class.\n\nParameters\n----------\ninputs : Inputs\n    The inputs.\noutputs : Outputs\n    The outputs."
            }
        },
        "class_attributes": {
            "logger": "logging.Logger",
            "metadata": "d3m.metadata.base.PrimitiveMetadata"
        },
        "instance_attributes": {
            "hyperparams": "d3m.metadata.hyperparams.Hyperparams",
            "random_seed": "int",
            "docker_containers": "typing.Dict[str, d3m.primitive_interfaces.base.DockerContainer]",
            "volumes": "typing.Dict[str, str]",
            "temporary_directory": "typing.Union[NoneType, str]"
        },
        "params": {}
    },
    "structural_type": "primitives.cnn.cnn.ConvolutionalNeuralNetwork",
    "digest": "043692d362f1b9f18a26444fbf7f3cf6f1a217dd2e21eacef9bc9c5788a6195d"
}
