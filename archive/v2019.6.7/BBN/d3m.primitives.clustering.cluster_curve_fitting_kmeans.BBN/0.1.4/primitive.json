{
  "id": "b358a1fd-8bf8-4991-935f-2f1806dae54d",
  "version": "0.1.4",
  "name": "Clustering for Curve Fitting",
  "description": "BBN D3M Curve Fitting Clusterer performs clustering of segments representations based on the k-means algorithm.The segments are expected to have constant duration (this can be achieved by using the polynomial approximation of variable-length segments, see SegmentCurveFitter.\nInput: List of lists of segmented sequence of polynomial coefficients, i.e. List( [ seg_length, num_features ], [ seg_length, num_features ], ...)\nOutput: List of arrays of cluster ids assigned to segments of shape [ num_segments ]\nApplications include: audio, time-series classification\n\nFor details, refer to Gish, H. and Ng, K., 1996, October. Parametric trajectory models for speech recognition. In Spoken Language, 1996. ICSLP 96. Proceedings., Fourth International Conference on (Vol. 1, pp. 466-469). IEEE.\nor Yeung, S.K.A., Li, C.F. and Siu, M.H., 2005, March. Sub-phonetic polynomial segment model for large vocabulary continuous speech recognition. In Acoustics, Speech, and Signal Processing, 2005. Proceedings.(ICASSP'05). IEEE International Conference on (Vol. 1, pp. I-193). IEEE.\n\nAttributes\n----------\nmetadata : PrimitiveMetadata\n    Primitive's metadata. Available as a class attribute.\nlogger : Logger\n    Primitive's logger. Available as a class attribute.\nhyperparams : Hyperparams\n    Hyperparams passed to the constructor.\nrandom_seed : int\n    Random seed passed to the constructor.\ndocker_containers : Dict[str, DockerContainer]\n    A dict mapping Docker image keys from primitive's metadata to (named) tuples containing\n    container's address under which the container is accessible by the primitive, and a\n    dict mapping exposed ports to ports on that address.\nvolumes : Dict[str, str]\n    A dict mapping volume keys from primitive's metadata to file and directory paths\n    where downloaded and extracted files are available to the primitive.\ntemporary_directory : str\n    An absolute path to a temporary directory a primitive can use to store any files\n    for the duration of the current pipeline run phase. Directory is automatically\n    cleaned up after the current pipeline run phase finishes.",
  "keywords": [],
  "source": {
    "name": "BBN",
    "contact": "mailto:prasannakumar.muthukumar@raytheon.com",
    "uris": [
      "https://github.com/BBN-E/d3m-bbn-primitives/blob/029fc54554c43d7462f79279c655e525603800b9/bbn_primitives/time_series/cluster_curve_fitting_kmeans.py",
      "https://github.com/BBN-E/d3m-bbn-primitives.git"
    ]
  },
  "installation": [
    {
      "type": "PIP",
      "package_uri": "git+https://github.com/BBN-E/d3m-bbn-primitives.git@029fc54554c43d7462f79279c655e525603800b9#egg=bbn_primitives"
    }
  ],
  "python_path": "d3m.primitives.clustering.cluster_curve_fitting_kmeans.BBN",
  "algorithm_types": [
    "K_MEANS_CLUSTERING"
  ],
  "primitive_family": "CLUSTERING",
  "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/primitive.json",
  "original_python_path": "bbn_primitives.time_series.cluster_curve_fitting_kmeans.ClusterCurveFittingKMeans",
  "primitive_code": {
    "class_type_arguments": {
      "Inputs": "d3m.container.list.List",
      "Outputs": "d3m.container.list.List",
      "Params": "bbn_primitives.time_series.cluster_curve_fitting_kmeans.Params",
      "Hyperparams": "bbn_primitives.time_series.cluster_curve_fitting_kmeans.Hyperparams"
    },
    "interfaces_version": "2019.6.7",
    "interfaces": [
      "unsupervised_learning.UnsupervisedLearnerPrimitiveBase",
      "base.PrimitiveBase"
    ],
    "hyperparams": {
      "n_init": {
        "type": "d3m.metadata.hyperparams.Hyperparameter",
        "default": 10,
        "structural_type": "int",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/ControlParameter",
          "https://metadata.datadrivendiscovery.org/types/ResourcesUseParameter"
        ],
        "description": "Number of initializations with different centroid seeds"
      },
      "n_clusters": {
        "type": "d3m.metadata.hyperparams.Hyperparameter",
        "default": 32,
        "structural_type": "int",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/TuningParameter",
          "https://metadata.datadrivendiscovery.org/types/ResourcesUseParameter"
        ],
        "description": "Number of clusters"
      },
      "max_iter": {
        "type": "d3m.metadata.hyperparams.Hyperparameter",
        "default": 300,
        "structural_type": "int",
        "semantic_types": [
          "https://metadata.datadrivendiscovery.org/types/ControlParameter",
          "https://metadata.datadrivendiscovery.org/types/ResourcesUseParameter"
        ],
        "description": "Maximum number of iterations"
      }
    },
    "arguments": {
      "hyperparams": {
        "type": "bbn_primitives.time_series.cluster_curve_fitting_kmeans.Hyperparams",
        "kind": "RUNTIME"
      },
      "random_seed": {
        "type": "int",
        "kind": "RUNTIME",
        "default": 0
      },
      "docker_containers": {
        "type": "typing.Union[NoneType, typing.Dict[str, d3m.primitive_interfaces.base.DockerContainer]]",
        "kind": "RUNTIME",
        "default": null
      },
      "timeout": {
        "type": "typing.Union[NoneType, float]",
        "kind": "RUNTIME",
        "default": null
      },
      "iterations": {
        "type": "typing.Union[NoneType, int]",
        "kind": "RUNTIME",
        "default": null
      },
      "produce_methods": {
        "type": "typing.Sequence[str]",
        "kind": "RUNTIME"
      },
      "inputs": {
        "type": "d3m.container.list.List",
        "kind": "PIPELINE"
      },
      "params": {
        "type": "bbn_primitives.time_series.cluster_curve_fitting_kmeans.Params",
        "kind": "RUNTIME"
      }
    },
    "class_methods": {
      "can_accept": {
        "arguments": {
          "method_name": {
            "type": "str"
          },
          "arguments": {
            "type": "typing.Dict[str, typing.Union[d3m.metadata.base.Metadata, type]]"
          },
          "hyperparams": {
            "type": "bbn_primitives.time_series.cluster_curve_fitting_kmeans.Hyperparams"
          }
        },
        "returns": "typing.Union[NoneType, d3m.metadata.base.DataMetadata]",
        "description": "Returns a metadata object describing the output of a call of ``method_name`` method under\n``hyperparams`` with primitive arguments ``arguments``, if such arguments can be accepted by the method.\nOtherwise it returns ``None`` or raises an exception.\n\nDefault implementation checks structural types of ``arguments`` expected arguments' types\nand ignores ``hyperparams``.\n\nBy (re)implementing this method, a primitive can fine-tune which arguments it accepts\nfor its methods which goes beyond just structural type checking. For example, a primitive might\noperate only on images, so it can accept numpy arrays, but only those with semantic type\ncorresponding to an image. Or it might check dimensions of an array to assure it operates\non square matrix.\n\nPrimitive arguments are a superset of method arguments. This method receives primitive arguments and\nnot just method arguments so that it is possible to implement it without a state between calls\nto ``can_accept`` for multiple methods. For example, a call to ``fit`` could during normal execution\ninfluences what a later ``produce`` call outputs. But during ``can_accept`` call we can directly have\naccess to arguments which would have been given to ``fit`` to produce metadata of the ``produce`` call.\n\nNot all primitive arguments have to be provided, only those used by ``fit``, ``set_training_data``,\nand produce methods, and those used by the ``method_name`` method itself.\n\nParameters\n----------\nmethod_name : str\n    Name of the method which would be called.\narguments : Dict[str, Union[Metadata, type]]\n    A mapping between argument names and their metadata objects (for pipeline arguments) or types (for other).\nhyperparams : Hyperparams\n    Hyper-parameters under which the method would be called during regular primitive execution.\n\nReturns\n-------\nDataMetadata\n    Metadata object of the method call result, or ``None`` if arguments are not accepted\n    by the method."
      }
    },
    "instance_methods": {
      "__init__": {
        "kind": "OTHER",
        "arguments": [
          "hyperparams",
          "random_seed",
          "docker_containers"
        ],
        "returns": "NoneType"
      },
      "fit": {
        "kind": "OTHER",
        "arguments": [
          "timeout",
          "iterations"
        ],
        "returns": "d3m.primitive_interfaces.base.CallResult[NoneType]",
        "description": "Arguments\n    - inputs: List( # Data\n                 List( # Segments\n                    [ deg, num_feats ], ...\n                 )\n               ),\n\nParameters\n----------\ntimeout : float\n    A maximum time this primitive should be fitting during this method call, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nCallResult[None]\n    A ``CallResult`` with ``None`` value."
      },
      "fit_multi_produce": {
        "kind": "OTHER",
        "arguments": [
          "produce_methods",
          "inputs",
          "timeout",
          "iterations"
        ],
        "returns": "d3m.primitive_interfaces.base.MultiCallResult",
        "description": "A method calling ``fit`` and after that multiple produce methods at once.\n\nParameters\n----------\nproduce_methods : Sequence[str]\n    A list of names of produce methods to call.\ninputs : Inputs\n    The inputs given to ``set_training_data`` and all produce methods.\ntimeout : float\n    A maximum time this primitive should take to both fit the primitive and produce outputs\n    for all produce methods listed in ``produce_methods`` argument, in seconds.\niterations : int\n    How many of internal iterations should the primitive do for both fitting and producing\n    outputs of all produce methods.\n\nReturns\n-------\nMultiCallResult\n    A dict of values for each produce method wrapped inside ``MultiCallResult``."
      },
      "get_params": {
        "kind": "OTHER",
        "arguments": [],
        "returns": "bbn_primitives.time_series.cluster_curve_fitting_kmeans.Params",
        "description": "Returns parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nParameters should include all data which is necessary to create a new instance of this primitive\nbehaving exactly the same as this instance, when the new instance is created by passing the same\nparameters to the class constructor and calling ``set_params``.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nReturns\n-------\nParams\n    An instance of parameters."
      },
      "multi_produce": {
        "kind": "OTHER",
        "arguments": [
          "produce_methods",
          "inputs",
          "timeout",
          "iterations"
        ],
        "returns": "d3m.primitive_interfaces.base.MultiCallResult",
        "description": "A method calling multiple produce methods at once.\n\nWhen a primitive has multiple produce methods it is common that they might compute the\nsame internal results for same inputs but return different representations of those results.\nIf caller is interested in multiple of those representations, calling multiple produce\nmethods might lead to recomputing same internal results multiple times. To address this,\nthis method allows primitive author to implement an optimized version which computes\ninternal results only once for multiple calls of produce methods, but return those different\nrepresentations.\n\nIf any additional method arguments are added to primitive's produce method(s), they have\nto be added to this method as well. This method should accept an union of all arguments\naccepted by primitive's produce method(s) and then use them accordingly when computing\nresults.\n\nThe default implementation of this method just calls all produce methods listed in\n``produce_methods`` in order and is potentially inefficient.\n\nIf primitive should have been fitted before calling this method, but it has not been,\nprimitive should raise a ``PrimitiveNotFittedError`` exception.\n\nParameters\n----------\nproduce_methods : Sequence[str]\n    A list of names of produce methods to call.\ninputs : Inputs\n    The inputs given to all produce methods.\ntimeout : float\n    A maximum time this primitive should take to produce outputs for all produce methods\n    listed in ``produce_methods`` argument, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nMultiCallResult\n    A dict of values for each produce method wrapped inside ``MultiCallResult``."
      },
      "produce": {
        "kind": "PRODUCE",
        "arguments": [
          "inputs",
          "timeout",
          "iterations"
        ],
        "returns": "d3m.primitive_interfaces.base.CallResult[d3m.container.list.List]",
        "singleton": false,
        "inputs_across_samples": [],
        "description": "Arguments:\n    - inputs: List( # Data\n                List( # Segments\n                  [ deg, num_feats ], ...\n                )\n              )\n\n\nReturns:\n    - List(d3m_ndarray)\n\nParameters\n----------\ninputs : Inputs\n    The inputs of shape [num_inputs, ...].\ntimeout : float\n    A maximum time this primitive should take to produce outputs during this method call, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nCallResult[Outputs]\n    The outputs of shape [num_inputs, ...] wrapped inside ``CallResult``."
      },
      "set_params": {
        "kind": "OTHER",
        "arguments": [
          "params"
        ],
        "returns": "NoneType",
        "description": "Sets parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nParameters\n----------\nparams : Params\n    An instance of parameters."
      },
      "set_training_data": {
        "kind": "OTHER",
        "arguments": [
          "inputs"
        ],
        "returns": "NoneType",
        "description": "Sets training data of this primitive.\n\nParameters\n----------\ninputs : Inputs\n    The inputs."
      }
    },
    "class_attributes": {
      "logger": "logging.Logger",
      "metadata": "d3m.metadata.base.PrimitiveMetadata"
    },
    "instance_attributes": {
      "hyperparams": "d3m.metadata.hyperparams.Hyperparams",
      "random_seed": "int",
      "docker_containers": "typing.Dict[str, d3m.primitive_interfaces.base.DockerContainer]",
      "volumes": "typing.Dict[str, str]",
      "temporary_directory": "typing.Union[NoneType, str]"
    },
    "params": {
      "cluster_centers_": "numpy.ndarray"
    }
  },
  "structural_type": "bbn_primitives.time_series.cluster_curve_fitting_kmeans.ClusterCurveFittingKMeans",
  "digest": "3354531c1a4ed0dca7eb8cd0cb2d8567e6c89fe941b3c1dacd7bdbfdd96d4f4c"
}
